# Rc Lights

Simple micropython script, which controls LED lights in RC car.
It reads PWM output from RC receiver and controls rear lights and turn signals. It consumes values from steering, throttle
and 3rd channel.

## Dependencies

``PWMCounter`` from https://github.com/phoreglad/pico-MP-modules/blob/main/PWMCounter/PWMCounter.py

## Author
Martin Klusoň (martin@kluson.cz)

## License
GPL v3
