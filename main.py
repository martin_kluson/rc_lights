# RC Lights
# Copyright (C) 2023  Martin Klusoň (martin@kluson.cz)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

LEFT_TURN_SIGNAL = 10
RIGHT_TURN_SIGNAL = 11

LEFT_TAIL_LIGHT = 12
RIGHT_TAIL_LIGHT = 13

AUX_SIGNAL = 1
SERVO_SIGNAL = 3
THR_SIGNAL = 5

TAIL_LIGHT_LOW_DUTY = 10000
TAIL_LIGHT_HIGHT_DUTY = 60000

INPUT_LOW = 80
INPUT_HIGHT = 100

from machine import Pin, PWM
from PWMCounter import PWMCounter
import _thread
import time


def set_tail_duty(left, right, duty):
    global lights_enabled
    
    if lights_enabled:
        left.duty_u16(duty)
        right.duty_u16(duty)
    else:
        left.duty_u16(0)
        right.duty_u16(0) 


def blink(led):
    led.value(1)        
    time.sleep(0.5)
    led.value(0)
    time.sleep(0.5)
    
def blink2(led1, led2):
    led1.value(1)
    led2.value(1)
    time.sleep(0.5)
    led1.value(0)
    led2.value(0) 
    time.sleep(0.5)
    

def turn_signals_thread():
    global turn_left
    global turn_right
    global lights_enabled
    global hazzard_lights
    
    left_turn_signal = Pin(LEFT_TURN_SIGNAL, Pin.OUT)
    right_turn_signal = Pin(RIGHT_TURN_SIGNAL, Pin.OUT)
    
    while True:
        if lights_enabled:
            if turn_left:
                blink(left_turn_signal)
            if turn_right:
                blink(right_turn_signal)
            if hazzard_lights:
                blink2(left_turn_signal, right_turn_signal)

def main():
    global turn_left
    global turn_right
    
    global lights_enabled
    global hazzard_lights
    
    brake = False
    reverse = False
    
    left_tail_light = PWM(Pin(LEFT_TAIL_LIGHT))
    right_tail_light = PWM(Pin(RIGHT_TAIL_LIGHT))
    
    left_tail_light.freq(1000)
    right_tail_light.freq(1000)
    
    left_tail_light.duty_u16(20000)
    right_tail_light.duty_u16(20000)
    
    aux_in_pin = Pin(AUX_SIGNAL, Pin.IN)
    aux_counter = PWMCounter(AUX_SIGNAL, PWMCounter.LEVEL_HIGH)
    aux_counter.set_div(256)
    aux_counter.start()
    
    servo_in_pin = Pin(SERVO_SIGNAL, Pin.IN)
    servo_counter = PWMCounter(SERVO_SIGNAL, PWMCounter.LEVEL_HIGH)
    servo_counter.set_div(256)
    servo_counter.start()
    
    thr_in_pin = Pin(THR_SIGNAL, Pin.IN)
    thr_counter = PWMCounter(THR_SIGNAL, PWMCounter.LEVEL_HIGH)
    thr_counter.set_div(256)
    thr_counter.start()

    aux_last_state = 0
    servo_last_state = 0
    thr_last_state = 0
    while True:
        if ~(x := aux_in_pin.value()) & aux_last_state:
            value = (aux_counter.read_and_reset() * 16) / 125
            if value < INPUT_LOW:
                hazzard_lights = True
            elif value > INPUT_HIGHT:
                lights_enabled = False
            else:
                lights_enabled = True
                hazzard_lights = False
        aux_last_state = x
        
        if ~(x := servo_in_pin.value()) & servo_last_state:
            value = (servo_counter.read_and_reset() * 16) / 125
            if value < INPUT_LOW:
                turn_left = True
            elif value > INPUT_HIGHT:
                turn_right = True
            else:
                turn_left = False
                turn_right = False
        servo_last_state = x
        
        if ~(y := thr_in_pin.value()) & thr_last_state:
            value = (thr_counter.read_and_reset() * 16) / 125
            if value < INPUT_LOW:
                if not reverse:
                    brake = True
                    
            elif value > INPUT_HIGHT:
                brake = False
                reverse = False
                    
            else:
                if brake:
                    brake = False
                    reverse = True
                
        thr_last_state = y
        
        if brake:
            set_tail_duty(left_tail_light, right_tail_light, TAIL_LIGHT_HIGHT_DUTY)
        else:
            set_tail_duty(left_tail_light, right_tail_light, TAIL_LIGHT_LOW_DUTY)
        


turn_left = False
turn_right = False

lights_enabled = True
hazzard_lights = False

_thread.start_new_thread(turn_signals_thread, ())
main()
